#!/bin/bash

###
### This script is based on content found at: https://firstgen-docs.giantswarm.io/guides/mysql-backup/
###

# where backups are saved to
backuppath=$BACKUP_PATH

# interval between backups in seconds
interval=$BACKUP_INTERVAL

# MySQL configuration
dbhost=$MYSQL_PORT_3306_TCP_ADDR
dbport=$MYSQL_PORT_3306_TCP_PORT
dbname=$DB_NAME
dbuser=$DB_USER
dbpass=$DB_PASSWORD

# pattern to create subdirectories from date elements,
# e. g. '%Y/%m/%d' or '%Y/%Y-%m-%d'
pathpattern=$PATH_DATEPATTERN

count=1

while [ 1 ]
do
	# set date-dependent path element
	datepath=`date +"$pathpattern"`

	# determine file name
	datetime=`date +"%Y-%m-%d_%H-%M"`
	filename=$backuppath/$datepath/$dbname_$datetime.sql

	echo "Writing backup No. $count for $dbhost:$dbport/$dbname to $filename.gz"

	mysqldump -h $dbhost -P $dbport -u $dbuser --password="$dbpass" $dbname > $filename

	gzip $filename

	# increment counter and for the time to pass by...
	count=`expr $count + 1`
	sleep $interval
done
