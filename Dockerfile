FROM debian:jessie

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -y -q && \
  apt-get install -y mysql-client-5.5 && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

ADD backup.sh /backup.sh
RUN chmod 0755 /backup.sh

ENTRYPOINT ["/backup.sh"]
